# Summary

* [Modulleitfaden](README.md)
* [Organisatorisches](0_Organisatorisches/README.md)
* [Kompetenzmatrix](1_Kompetenzmatrix/README.md)
  * [Fragenkatalog](1_Kompetenzmatrix/Fragenkatalog.md)
  * [AG1](2_Unterrichtsressourcen/AG1/README.md)
  * [AG2](2_Unterrichtsressourcen/AG2/README.md)
  * [AF1](2_Unterrichtsressourcen/AF1/README.md)
  * [AF2](2_Unterrichtsressourcen/AF2/README.md)
  * [AE1](2_Unterrichtsressourcen/AE1/README.md)
  * [AE2](2_Unterrichtsressourcen/AE2/README.md)
* [Umsetzungsvorschlag](3_Umsetzungsvorschlag/README.md)