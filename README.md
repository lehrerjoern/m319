# M319-Applikationen_entwerfen_und_implementieren

Lehrjahr-1

### Modulbeschreibung:

Applikationen entwerfen und implementieren

Die Lernenden kennen die Grundlagen des Programmierens, deren Herkunft, ihrer Voraussetzungen und sind in der Lage im Beruflichen Umfeld Probleme zu verstehen und Lösungen dafür zu entwickeln.



#### Kompetenzmatrix:

[Kompetenzmatrix](https://gitlab.com/modulentwicklungzh/cluster-api/m319/-/blob/master/1_Kompetenzmatrix/README.md)



#### Umsetzungsvorschlag:

[Umsetzungsvorschlag](https://gitlab.com/modulentwicklungzh/cluster-api/m319/-/blob/master/3_Umsetzungsvorschlag/README.md)



#### Unterrichtsresourcen:

##### Basis Template für Implementierungen

* [Template](https://gitlab.com/learnmodulentwicklung/cluster-api/m319/-/blob/master/2_Unterrichtsressourcen/Allgemein/Template.java)



##### Interationen while, do while

- [Input/Merkblatt](https://gitlab.com/modulentwicklungzh/cluster-api/m319/-/blob/master/2_Unterrichtsressourcen/Iterationen/README.md)
- [Uebungen](https://gitlab.com/modulentwicklungzh/cluster-api/m319/-/blob/master/2_Unterrichtsressourcen/Iterationen/WhileAufgaben.md)
- [Überprüfen](https://gitlab.com/modulentwicklungzh/cluster-api/m319/-/blob/master/2_Unterrichtsressourcen/Iterationen/WhileCheckpoint.md)

